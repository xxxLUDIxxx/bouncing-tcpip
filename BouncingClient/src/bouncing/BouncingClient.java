package bouncing;

import java.io.*;
import java.net.Socket;

public class BouncingClient {
    private final String serverName;
    private final int serverPort;
    private Socket socket;
    private OutputStream serverOut;
    private InputStream serverIn;
    private BufferedReader bufferedIn;
    private int height;
    private int width;

    public BouncingClient(String serverName, int serverPort, int height, int width) {
        this.serverName = serverName;
        this.serverPort = serverPort;
        this.height = height;
        this.width = width;

        if (!connect()) {
            System.err.println("Connection failed.");
        } else {
            System.out.println("Connection successful.");
            startMessageReader();
            try {
                send("start:" + height + ":" + width);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*public static void main(String[] args) throws IOException {
        BouncingClient client = new BouncingClient("localhost", 8818);
        if (!client.connect()) {
            System.err.println("Connection failed.");
        } else {
            System.out.println("Connection successful.");
            client.startMessageReader();
            client.send("start:" + client.height + ":" + client.width);
        }
    }*/

    private void startMessageReader() {
        Thread t = new Thread() {
            @Override
            public void run() {
                readServerMessageLoop();
            }

        };
        t.start();
    }

    private void readServerMessageLoop() {
        String line;
        try {
            while ((line = bufferedIn.readLine()) != null) {
                String[] tokens = line.split(":");
                if ( tokens.length > 0) {
                    String cmd = tokens[0];
                    if ("init".equalsIgnoreCase(cmd)) {
                        int x = Integer.parseInt(tokens[1]);
                        int y = Integer.parseInt(tokens[2]);
                        int r = Integer.parseInt(tokens[3]);

                        System.out.println("INITIALIZED BALL: X = " + x +"; Y = " + y + "; R = " + r + ";");
                        send("move");
                        System.out.println("I requested a move");
                    } else if ("moved".equalsIgnoreCase(cmd)) {
                        int x = Integer.parseInt(tokens[1]);
                        int y = Integer.parseInt(tokens[2]);
                        System.out.println("MOVED BALL TO: X = " + x +"; Y = " + y + ";");
                        send("move");
                    } else {
                        System.out.println("You've fucked up:" + line);
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean connect() {
        try {
            this.socket = new Socket(serverName, serverPort);
            System.out.println("Client port: " + socket.getLocalPort());
            this.serverOut = socket.getOutputStream();
            this.serverIn = socket.getInputStream();
            this.bufferedIn = new BufferedReader(new InputStreamReader(serverIn));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void send(String msg) throws IOException {
        serverOut.write((msg + "\n").getBytes());
    }
}