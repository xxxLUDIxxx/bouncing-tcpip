package bouncing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class Board extends JPanel implements ActionListener {
    private final static float PIXEL = 16;

    private static int speed = 12;
    private static int width;
    private static int height;
    private static int radius;

    private Timer timer;

    public Board(int width, int height) {
        setBackground(Color.BLACK);
        setFocusable(true);

        setPreferredSize(new Dimension(width, height));

        initializeGame();
    }

    private void initializeGame() {
        timer = new Timer(speed,this);
        timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawGradient(g);

        draw(g);
    }

    void draw(Graphics g) {
        g.setColor(Color.RED);
        drawCenteredBall(g, width, height, radius);
    }

    void drawCenteredBall(Graphics g,int x, int y, int r) {
        x = x-(r/2);
        y = y-(r/2);
        g.fillOval(x,y,r,r);
    }

    void drawGradient(Graphics g) {
        final BufferedImage image;
        float[] Color1 = {255, 255, 0};
        float[] Color2 = {42, 106, 255};
        image = (BufferedImage) createImage(width, height);
        WritableRaster raster = image.getRaster();
        for (int row = 0; row < height; row++) {
            float t = (float) row / height;
            for (int col = 0; col < width; col++) {
                float[] InterpolatedColor = interpolate(Color1, Color2, t);
                raster.setPixel(col, row, InterpolatedColor);
            }
        }

        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
    }

    private float[] interpolate(float[] color1, float[] color2, float t) {
        float[] InterpolatedColor = {0,0,0};
        InterpolatedColor[0] = color1[0] + (color2[0] - color1[0]) * t;
        InterpolatedColor[1] = color1[1] + (color2[1] - color1[1]) * t;
        InterpolatedColor[0] = color1[2] + (color2[2] - color1[2]) * t;

        return InterpolatedColor;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        repaint();
    }
}
