package bouncing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class StartWindow extends JFrame {
    JTextField heightField = new JTextField();
    JTextField widthField = new JTextField();
    JButton startButton = new JButton("Start");
    BouncingClient client;

    public StartWindow() {
        super("Start Bouncing Ball");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        p.add(widthField);
        p.add(heightField);
        p.add(startButton);

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                doStart();
            }
        });

        getContentPane().add(p, BorderLayout.CENTER);

        pack();

        setVisible(true);
    }

    private void doStart() {

        if( !widthField.getText().isEmpty() && !heightField.getText().isEmpty()) {
            int width = Integer.parseInt(widthField.getText());
            int height = Integer.parseInt(heightField.getText());
            this.client = new BouncingClient("localhost", 8818, height,width);
            if (!client.connect()) {
                System.err.println("Connection failed.");
            } else {
                System.out.println("Connection successful.");
                try {
                    client.send("start:" + width + ":" + height);
                    // #TODO
                    // Init Board
                    JFrame frame = new JFrame("Bouncing Ball Game");

                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    // Create JPanel
                    Board board = new Board(width, height);
                    frame.getContentPane().add(board, BorderLayout.CENTER);
                    frame.pack();
                    frame.setVisible(true);

                    setVisible(false);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        StartWindow startWindow = new StartWindow();
        startWindow.setVisible(true);
    }
}