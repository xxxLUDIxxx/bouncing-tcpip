package bouncing;

import java.io.*;
import java.net.Socket;

public class ServerWorker extends Thread {
    private static int boardWidth;
    private static int boardHeight;

    private static int PIXELSIZE = 16;

    private final Server server;
    private final Socket clientSocket;

    // #TODO
    // Colors controller
    /*float[] Color1 = {255, 255, 0};
    float[] Color2 = {42, 106, 255};*/

    private OutputStream outputStream;
    private Ball ball;

    public ServerWorker(Server server, Socket clientSocket) {
        this.server = server;
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        try {
            System.out.println("I'm thread " + currentThread());
            handleClientSocket();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void handleClientSocket() throws IOException, InterruptedException {
        InputStream inputStream = clientSocket.getInputStream();
        this.outputStream = clientSocket.getOutputStream();

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] tokens = line.split(":");
            if (tokens.length > 0) {
                String cmd = tokens[0];
                if ("start".equalsIgnoreCase(cmd)) {
                    System.out.println("I received start! " + tokens[1] + ":" + tokens[2]);
                    handleStart(tokens);
                } else if ("move".equalsIgnoreCase(cmd)) {
                    handleMove();
                } else if ("close".equalsIgnoreCase(cmd)) {
                    handleClose();
                } else {
                    String msg = "Unknown command: " + cmd + "\n";
                    outputStream.write(msg.getBytes());
                }
            }
        }
    }

    private void handleStart(String[] tokens) throws IOException {
        if (tokens.length != 3) {
            String msg = "Unknown dimensions";
            outputStream.write(msg.getBytes());
        } else {
            boardWidth = Integer.parseInt(tokens[1]);
            boardHeight = Integer.parseInt(tokens[2]);

            this.ball = new Ball(boardWidth, boardHeight);
            System.out.println("Initializing Ball on: " + ball.getBallX() + " " + ball.getBallY());

            // #TODO
            // Generate colors
            String startBallMsg = "init:" + ball.getBallX() + ":" + ball.getBallY() + ":" + ball.getBallR() + "\n";
            System.out.println(startBallMsg);
            send(startBallMsg);

            System.out.println("GAME STARTED");
        }
    }

    private void handleMove() throws IOException {
        checkCollisions();
        this.ball.move(PIXELSIZE);
        String msg = generateBallMsg();
        send(msg);
    }

    private void handleClose() throws IOException {
        // #TODO
        clientSocket.close();
    }

    public void send(String msg) throws IOException {
        outputStream.write(msg.getBytes());
        outputStream.flush();
    }

    public String generateBallMsg() {
        String ballMsg = "moved:" + ball.getBallX() + ":" + ball.getBallY() + "\n";
        return ballMsg;
    }

    void checkCollisions() {
        if (ball.getBallY() >= boardHeight) {
            ball.setMovingUp();
        }

        if (ball.getBallY() <= 0) {
            ball.setMovingDown();
        }

        if (ball.getBallX() >= boardWidth) {
            ball.setMovingLeft();
        }

        if (ball.getBallX() < 0) {
            ball.setMovingRight();
        }
    }
}