package bouncing;

import java.util.Random;

public class Ball {

    Random randomGenerator;

    private int ballX;
    private int ballY;
    private int ballR;

    public Ball(int ballX, int ballY) {
        this.randomGenerator = new Random();
        this.ballX = randomGenerator.nextInt(ballX);
        this.ballY = randomGenerator.nextInt(ballY);
        this.ballR = randomGenerator.nextInt(35) + 10;

        setDirection(ballX, ballY);
    }

    private enum Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    private Direction horizontalDirection;
    private Direction verticalDirection;

    private void setDirection(int width, int height) {
        if(ballX < (width / 2)) {
            setMovingRight();
        } else {
            setMovingLeft();
        }

        if(ballY < (height/2)) {
            setMovingUp();
        } else {
            setMovingDown();
        }
    }

    public int getBallX() {
        return ballX;
    }

    public int getBallY() {
        return ballY;
    }

    public int getBallR() { return ballR; }


    // LEFT
    public boolean isMovingLeft() {
        if (horizontalDirection == Direction.LEFT) {
            return true;
        } else {
            return false;
        }
    }

    public void setMovingLeft() {
        this.horizontalDirection = Direction.LEFT;
    }
    // RIGHT
    public boolean isMovingRight() {
        if (horizontalDirection == Direction.RIGHT) {
            return true;
        } else {
            return false;
        }
    }

    public void setMovingRight() {
        this.horizontalDirection = Direction.RIGHT;
    }
    // UP
    public boolean isMovingUp() {
        if (verticalDirection == Direction.UP) {
            return true;
        } else {
            return false;
        }
    }

    public void setMovingUp() {
        this.verticalDirection = Direction.UP;
    }

    public boolean isMovingDown() {
        if (verticalDirection == Direction.DOWN) {
            return true;
        } else {
            return false;
        }
    }

    public void setMovingDown() {
        this.verticalDirection = Direction.DOWN;
    }

    public void move(int pixelSize) {
        // Left
        if (isMovingLeft()) {
            ballX -= pixelSize;
        }
        // Right
        if (isMovingRight()) {
            ballX += pixelSize;
        }
        // Up
        if (isMovingUp()) {
            ballY -= pixelSize;
        }
        // Down
        if (isMovingDown()) {
            ballY += pixelSize;
        }
    }
}