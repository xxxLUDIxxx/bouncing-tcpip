package bouncing;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


public class Server extends Thread {
    private final int serverPort;

    private ArrayList<ServerWorker> workers = new ArrayList<>();

    public Server(int port) {
        this.serverPort = port;
    }

    public List<ServerWorker> getWorkerList() {
        return workers;
    }

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(serverPort);
            while(true) {
                System.out.println("About to accept the client's connection");
                Socket clientSocket = serverSocket.accept();
                System.out.println("Accepted connection from " + clientSocket);
                // Worker
                ServerWorker worker = new ServerWorker(this, clientSocket);
                addWorker(worker);

                worker.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addWorker(ServerWorker worker) {
        workers.add(worker);
    }

    public void removeWorker(ServerWorker worker) {
        workers.remove(worker);
    }
}